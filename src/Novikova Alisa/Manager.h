﻿#pragma once
#include "Employee.h"
#include "Interface.h"

class Manager : public Employee, public Project
{
protected:
	float ratio = 0.05;
	string project;
	int budget;
	
public:
	Manager() {};

	Manager(int id, string name, string project, int budget)
	{
		this->id = id;
		this->name = name;
		this->project = project;
		this->budget = budget;
		
	}

	int PayProject(int budget, double ratio) { return budget * ratio; }
	
	void Pay()
		 {
		payment = PayProject(budget, ratio);
		 }
};

class ProjectManager : public Manager, public Heading
{
protected:
	float ratio = 0.17;
	string project;
	int budget;
	int pay = 2000; 
	int numberEmp;
public:
	ProjectManager() {};

	ProjectManager(int id, string name, string project, int budget, int numberEmp)
	{
		this->id = id;
		this->name = name;
		this->project = project;
		this->budget = budget;
		this->numberEmp = numberEmp;
	}

	int PayHeading(int pay, int numberEmp) { return pay * numberEmp; }

	void Pay()
	{
		payment = PayProject(budget, ratio) + PayHeading(pay, numberEmp);
	}
};

class SeniorManager : public Manager, public Project
{
protected:
	float ratio = 0.18;
	string project;
	int budget;
public:
	SeniorManager() { };

	SeniorManager(int id, string name, string project, int budget)
	{
		this->id = id;
		this->name = name;
		this->project = project;
		this->budget = budget;
	}

	int PayProject(int budget, double ratio) { return budget * ratio; }

	void Pay()
	{
		payment = PayProject(budget, ratio);
	}
};