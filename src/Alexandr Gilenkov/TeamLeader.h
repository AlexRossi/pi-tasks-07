#pragma once
#include "Programmer.h"
#include "Heading.h"
class TeamLeader : public Programmer, public Heading
{
protected:
	int quality;
public:
	TeamLeader(int id = 0, string fio = "", int base = 0, string project = "", int quality = 0, double contribution = 0, int worktime = 0, int payment = 0) {
		this->id = id;
		this->fio = fio;
		this->base = base;
		this->project = project;
		this->quality = quality;
		this->worktime = worktime;
		this->payment = payment;
		this->contribution = contribution;
	}

	void SetPayment() {
		this->payment = (EstimationWorkTime(this->worktime, this->base) +
			EstimationProject(1000, contribution) +
			EstimationHeading(quality))*0.95;
	}

	~TeamLeader() {}
};
