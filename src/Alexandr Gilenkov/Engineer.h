#pragma once
#include "Employee.h"
#include "WorkTime.h"
#include "Project.h"
class Engineer :public Employee, public WorkTime, public Project
{
protected:
	int base;
	double contribution;
	string project;
public:
	Engineer() {}

	void SetBase(int base) {
		this->base = base;
	}

	int GetBase() const {
		return base;
	}

	void SetContribution(double contribution) {
		this->contribution = contribution;
	}

	double GetContriution() {
		return contribution;
	}

	void SetProject(string project) {
		this->project = project;
	}

	string GetProject() {
		return project;
	}

	~Engineer() {}
};
