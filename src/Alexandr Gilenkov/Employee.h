#pragma once
#include <string>
using namespace std;
class Employee
{
protected:
	int id;
	string fio;
	int worktime;
	int payment;
public:
	Employee() {}

	void SetId(int id) {
		this->id = id;
	}

	void SetFio(string fio) {
		this->fio = fio;
	}

	void SetWorkTime(int worktime) {
		this->worktime = worktime;
	}

	virtual void SetPayment() = 0;

	int GetId() const {
		return id;
	}

	string GetFio() const {
		return fio;
	}

	int GetWorkTime() const {
		return worktime;
	}

	int GetPayment() const {
		return payment;
	}

	~Employee() {}
};
