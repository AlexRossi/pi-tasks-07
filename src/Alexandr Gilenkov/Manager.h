#pragma once
#include "Employee.h"
#include "Project.h"
class Manager : public Employee, public Project
{
protected:
	double contribution;
	string project;
public:
	Manager(int id = 0, string fio = "", string project = "", double contribution = 0, int worktime = 0, int payment = 0) {
		this->id = id;
		this->fio = fio;
		this->project = project;
		this->worktime = worktime;
		this->payment = payment;
		this->contribution = contribution;
	}

	void SetContribution(double contribution) {
		this->contribution = contribution;
	}

	double GetContriution() {
		return contribution;
	}

	void SetProject(string project) {
		this->project = project;
	}

	string GetProject() {
		return project;
	}

	void SetPayment() {
		this->payment = EstimationProject(1000, contribution)*0.9;
	}

	~Manager() {}
};
