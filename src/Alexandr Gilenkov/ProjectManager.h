#pragma once
#include "Manager.h"
#include "Heading.h"
class ProjectManager : public Manager, public Heading
{
protected:
	int quality;
public:
	ProjectManager(int id = 0, string fio = "", string project = "", int quality = 0, double contribution = 0, int worktime = 0, int payment = 0) {
		this->id = id;
		this->fio = fio;
		this->project = project;
		this->quality = quality;
		this->worktime = worktime;
		this->payment = payment;
		this->contribution = contribution;
	}

	void SetPayment() {
		this->payment = (EstimationProject(1000, contribution) + EstimationHeading(quality))*0.9;
	}

	~ProjectManager() {}
};
