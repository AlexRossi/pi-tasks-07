#pragma once
#include "Engineer.h"
class Programmer : public Engineer
{
public:
	Programmer(int id = 0, string fio = "", int base = 0, string project = "", double contribution=0, int worktime=0, int payment=0){
		this->id = id;
		this->fio = fio;
		this->base = base;
		this->project = project;
		this->worktime = worktime;
		this->payment = payment;
		this->contribution = contribution;
	}

	void SetPayment() {
		this->payment = (EstimationWorkTime(this->worktime, this->base) + EstimationProject(1000, contribution))*0.95;
	}

	~Programmer(){}
};

